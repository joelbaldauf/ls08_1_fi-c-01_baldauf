import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author dariush
 */
public class CSVHandler {

    private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
    private String delimiter = ";";

    // Constructor 1
    public CSVHandler() {
    }

    // Constructor 2
    public CSVHandler(String delimiter, String file) {
        super();
        this.delimiter = delimiter;
        this.file = file;
    }

    // Begin Methods

    public List<Schueler> getAll() {
        Schueler s = null;
        List<Schueler> students = new ArrayList<>();
        int[] positions = new int[5];
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            assert reader != null;
            String header = reader.readLine();
            ArrayList<String> headerList = new ArrayList<>(Arrays.asList(header.split(delimiter)));
            positions[0] = headerList.indexOf("Name");
            positions[1] = headerList.indexOf("Vorname");
            positions[2] = headerList.indexOf("joker");
            positions[3] = headerList.indexOf("blamiert");
            positions[4] = headerList.indexOf("fragen");

            String line;
            while ((line = reader.readLine()) != null) {
                String[] daten;
                daten = line.split(delimiter);
                try {
                    s = new Schueler(daten[positions[0]] + " " + daten[positions[1]], Integer.parseInt(daten[positions[2]]), Integer.parseInt(daten[positions[3]]), Integer.parseInt(daten[positions[4]]));
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Eingabedatei falsch formatiert");
                    System.exit(0);
                }
                students.add(s);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return students;
    }

    public void printAll(List<Schueler> students) {
        String format = "%-17s %-10s %-10s %-10s\n";
        System.out.printf(format, "Name", "Joker", "Blamieren", "Frage");
        for (Schueler s : students) {
            System.out.printf(format, s.getName(), s.getJoker(), s.getBlamiert(), s.getFragen());
        }
    }
}