package Auftrag1;/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml"
 * 					und sichern Sie es als XML in eine Datei
 * 					mit dem Filename "buchhandlung.xml".
 *
 *
 */


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class WriteBookstoreData1 {

    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dbuilder = factory.newDocumentBuilder();
            Document doc = dbuilder.newDocument();

            Element buchhandlung = doc.createElement("buchhandlung");
            doc.appendChild(buchhandlung);

            Element buch = doc.createElement("buch");
            buch.setAttribute("lang", "de");
            Element titel = doc.createElement("titel");
            titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));
            buch.appendChild(titel);

            buchhandlung.appendChild(buch);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(new File("buchhandlung.xml"));

            transformer.transform(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
