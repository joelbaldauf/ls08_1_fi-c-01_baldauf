package Auftrag3;/* Arbeitsauftrag:  Speichern Sie die Liste der B�cher
 * 					(buchliste) in die Datei "buchhandlung.xml".
 * 					Dabei gehen Sie wie folgt vor:
 *                  - Erstellen Sie ein DOM-Dokument  
 * 					- sichern Sie es als XML in die Datei "buchhandlung.xml".
 * 
 * 	Hinweis: Die Struktur der Ergebnisdatei soll der Datei 
 *           "Vorgabe_f�r_Ausgabedatei.xml" entsprechen. 			
 *               
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData3 {

	public static void main(String[] args) {

		List<Buch> buchliste = new ArrayList<>();
		buchliste.add(new Buch("Everyday Italian", "Giada De Laurentiis", 30.0));
		buchliste.add(new Buch("Harry Potter", "J K. Rowling", 29.99));
		buchliste.add(new Buch("XQuery Kick Start", "James McGovern", 49.99));
		buchliste.add(new Buch("Learning XML", "Erik T. Ray", 39.95));

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.newDocument();

			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);

			for (Buch buch:buchliste
				 ) {
				Element buchel = doc.createElement("buch");
				buchel.setAttribute("lang", "de");
				Element titel = doc.createElement("titel");
				titel.appendChild(doc.createTextNode(buch.getTitel()));
				buchel.appendChild(titel);
				Element autor = doc.createElement("autor");
				autor.appendChild(doc.createTextNode(buch.getAutor()));
				buchel.appendChild(autor);
				Element preis = doc.createElement("preis");
				preis.appendChild(doc.createTextNode(String.format("%4.2f", buch.getPreis()).replace(",",".")));
				buchel.appendChild(preis);
				buchhandlung.appendChild(buchel);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("buchhandlung.xml"));

			transformer.transform(source, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
