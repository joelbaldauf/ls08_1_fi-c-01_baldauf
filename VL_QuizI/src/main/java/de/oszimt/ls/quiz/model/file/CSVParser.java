package de.oszimt.ls.quiz.model.file;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.oszimt.ls.quiz.model.Model;
import de.oszimt.ls.quiz.model.Schueler;
import de.oszimt.ls.quiz.model.Spielstand;
import org.w3c.dom.Element;

public class CSVParser {
	private File datei;
	private String delimiter = ";";

	/**
	 * Erstelle XML Datei
	 *
	 * @param pfad, Pfad zur Datei
	 */
	public CSVParser(String pfad) {
		this.datei = new File(pfad);
	}

	/**
	 * Lädt die XML Datei
	 * 
	 * @return Model der XML-Datei
	 */
	public Model laden() {
		Model model = new Model();

		// Schueler auslesen
		Schueler s = null;
		List<Schueler> students = new ArrayList<>();
		int[] positions = new int[5];
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(datei));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			assert reader != null;
			String header = reader.readLine();
			ArrayList<String> headerList = new ArrayList<>(Arrays.asList(header.split(delimiter)));
			positions[0] = headerList.indexOf("Name");
			positions[1] = headerList.indexOf("Vorname");
			positions[2] = headerList.indexOf("joker");
			positions[3] = headerList.indexOf("blamiert");
			positions[4] = headerList.indexOf("fragen");

			String line;
			while ((line = reader.readLine()) != null) {
				String[] daten;
				daten = line.split(delimiter);
				try {
					s = new Schueler(daten[positions[0]], daten[positions[1]], Integer.parseInt(daten[positions[2]]), Integer.parseInt(daten[positions[3]]), Integer.parseInt(daten[positions[4]]));
				} catch (IndexOutOfBoundsException e) {
					System.out.println("Eingabedatei falsch formatiert");
					System.exit(0);
				}
				students.add(s);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (Schueler student : students) {
			model.getAlleSchueler().add(student);
		}
		model.setSpielstand(new Spielstand("Lehrer", 0, "Schüler", 0));
		return model;
	}
}
