package Auftrag5;

public class Buchhandlung {
    private String name;
    private String tel;
    private String fax;
    private Adresse adresse;

    public Buchhandlung(String name, String tel, String fax, Adresse adresse) {
        this.name = name;
        this.tel = tel;
        this.fax = fax;
        this.adresse = adresse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
}
