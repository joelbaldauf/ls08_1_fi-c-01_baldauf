package Auftrag5;

/* Auftrag 5) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung5.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Straße",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	},
	"buchliste": [
		{
			"titel": "Java ist auch eine Insel",
			"jahr": 1998,
			"preis": 49.9,
			"Autorenliste": [
				"Christian Ullenboom",
				"Paul Fuchs"
			]
		},
		{
			"titel": "XQuery Kick Start",
			"jahr": 2020,
			"preis": 17.99,
			"Autorenliste": [
				"James McGovern",
				"Per Bothner",
				"Kurt Cagle",
				"James Linn"
			]
		}
	]
}
*/

import javax.json.*;
import java.io.FileWriter;
import java.util.ArrayList;

public class WriteBookstoreDataJason {

	public static void main(String[] args) {
		Adresse adresse = new Adresse("Haarlemer Straße", "23-27", "12359", "Berlin");
		Buchhandlung buchhandlung = new Buchhandlung("OSZIMT Buchhandlung", "030-225027-800", "030-225027-809", adresse);

		ArrayList<Buch> buchliste = new ArrayList<Buch>();
		ArrayList<String> autorenliste = new ArrayList<String>();

		autorenliste.add("Christian Ullenboom");
		autorenliste.add("Paul Fuchs");
		buchliste.add(new Buch("Java ist auch eine Insel", 1998, 49.90, autorenliste));

		autorenliste = new ArrayList<String>();
		autorenliste.add("James McGovern");
		autorenliste.add("Per Bothner");
		autorenliste.add("Kurt Cagle");
		autorenliste.add("James Linn");
		buchliste.add(new Buch("XQuery Kick Start", 2020, 17.99, autorenliste));

		JsonObjectBuilder builderAdresse = Json.createObjectBuilder();
		builderAdresse.add("strasse", adresse.getStrasse());
		builderAdresse.add("hausnummer", adresse.getHausnummer());
		builderAdresse.add("plz", adresse.getPlz());
		builderAdresse.add("ort", adresse.getOrt());
		JsonObject joAdresse = builderAdresse.build();

		JsonArrayBuilder builderBuchArray = Json.createArrayBuilder();
		for (int i = 0; i < buchliste.size(); i++) {
			JsonObjectBuilder builderBuch = Json.createObjectBuilder();
			builderBuch.add("titel", buchliste.get(i).getTitel());
			builderBuch.add("jahr", buchliste.get(i).getJahr());
			builderBuch.add("preis", buchliste.get(i).getPreis());

			JsonArrayBuilder builderAutorenArray = Json.createArrayBuilder();
			for (int j = 0; j < buchliste.get(i).getAutorenliste().size(); j++) {
				builderAutorenArray.add(j, buchliste.get(i).getAutorenliste().get(j));
			};
			JsonArray jsonAutorenArray = builderAutorenArray.build();

			builderBuch.add("Autorenliste", jsonAutorenArray);
			JsonObject buch = builderBuch.build();
			builderBuchArray.add(i, buch);
		}
		JsonArray jsonBuchArray = builderBuchArray.build();

		JsonObjectBuilder builderBuchhandlung = Json.createObjectBuilder();
		builderBuchhandlung.add("name", buchhandlung.getName());
		builderBuchhandlung.add("tel", buchhandlung.getTel());
		builderBuchhandlung.add("fax", buchhandlung.getFax());
		builderBuchhandlung.add("adresse", joAdresse);
		builderBuchhandlung.add("buchliste", jsonBuchArray);
		JsonObject joBuchhandlung = builderBuchhandlung.build();

		FileWriter fw;
		try {
			fw = new FileWriter("book5.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(joBuchhandlung);
			fw.close();
			jw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
