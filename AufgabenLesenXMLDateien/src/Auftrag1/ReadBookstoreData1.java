package Auftrag1;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class ReadBookstoreData1 {

    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder dbuilder = factory.newDocumentBuilder();
            Document doc = dbuilder.parse("src/Auftrag1/buchhandlung.xml");


            NodeList titelList = doc.getElementsByTagName("titel");

            Node titelNode = titelList.item(0);

            System.out.println(titelNode.getNodeName() + ": " + titelNode.getTextContent());

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

    }

}
