package Auftrag5;

 /* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie benötigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Objects;

public class ReadBookstoreData5 {

	public static void main(String[] args) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse("src/Auftrag5/buchhandlung.xml");

			Node buch = doc.getElementsByTagName("buch").item(2);
			NodeList buchdetails = buch.getChildNodes();
			Node buchtitel = buchdetails.item(1);
			System.out.printf("%-10s %s\n","Buchtitel:", buchtitel.getTextContent());
			System.out.println("Autoren:");

			int ia = 0;
			for (int i = 0; i < buchdetails.getLength(); i++) {
				if (Objects.equals(buchdetails.item(i).getNodeName(), "autor")) {
					ia++;
					System.out.printf("%12d. %s %s\n", ia, "autor:", buchdetails.item(i).getTextContent());
				}
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}

	}

}
